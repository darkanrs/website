'use client'

import { useState, useEffect } from 'react'
import Link from 'next/link'
import { ChevronDown, Users, LogIn } from 'lucide-react'
import { LoginModal } from './LoginModal'
import { UserMenu } from './UserMenu'
import { useAuth } from '@/hooks/useAuth'

export default function Header() {
  const [showModal, setShowModal] = useState(false)
  const [playerCount, setPlayerCount] = useState(0)
  const { isLoggedIn, user, handleLogin, handleLogout } = useAuth()

  useEffect(() => {
    const fetchPlayerCount = async () => {
      try {
        const response = await fetch("https://api.darkan.org/v1/world/online")
        const data = await response.json()
        setPlayerCount(data.count)
      } catch (error) {
        console.error('Failed to fetch player count:', error)
      }
    }
    fetchPlayerCount()
  }, [])

  const handleLoginSubmit = async (username: string, password: string) => {
    await handleLogin(username, password)
  }

  return (
    <nav className="navbar is-fixed-top is-spaced has-background-black" role="navigation" aria-label="main navigation">
      <div className="container">
        <div className="navbar-brand">
          <Link href="/" className="navbar-item is-size-5 has-text-weight-semibold has-text-warning">
            Darkan
          </Link>
        </div>

        <div className="navbar-menu is-active">
          <div className="navbar-end">
            <div className="navbar-item has-dropdown is-hoverable">
              <a className="navbar-link is-arrowless has-text-grey-lighter">
                Explore
                <span className="icon is-small ml-1">
                  <ChevronDown size={16} />
                </span>
              </a>

              <div className="navbar-dropdown is-right has-background-black">
                <a href="https://discordapp.com/invite/Z32ggEB"
                   target="_blank"
                   rel="noopener noreferrer"
                   className="navbar-item has-text-grey-lighter">
                  Discord
                </a>
                <a href="https://gitlab.com/darkanrs"
                   target="_blank"
                   rel="noopener noreferrer"
                   className="navbar-item has-text-grey-lighter">
                  Source Code
                </a>
              </div>
            </div>

            <Link href="/highscores" className="navbar-item has-text-grey-lighter">
              Highscores
            </Link>

            <a href="https://gitlab.com/darkanrs/client-loader/-/releases"
               target="_blank"
               rel="noopener noreferrer"
               className="navbar-item has-text-warning">
              Download
            </a>

            <div className="navbar-item">
              <div className="box py-2 px-4 my-0 has-background-black-ter has-text-grey-lighter">
                <span className="icon mr-2">
                  <Users />
                </span>
                <span className="has-text-weight-semibold">{playerCount}</span>
              </div>
            </div>

            <div className="navbar-item">
              {isLoggedIn ? (
                <UserMenu username={user || ''} onLogout={handleLogout} />
              ) : (
                <button
                  onClick={() => setShowModal(true)}
                  className="button is-ghost has-text-grey-lighter"
                >
                  <span className="icon mr-2">
                    <LogIn />
                  </span>
                  <span>Login</span>
                </button>
              )}
            </div>
          </div>
        </div>
      </div>

      {showModal && (
        <LoginModal
          onClose={() => setShowModal(false)}
          onLogin={handleLoginSubmit}
        />
      )}
    </nav>
  )
}