import Link from 'next/link'

export default function Footer() {
  const linkClasses = "has-text-grey-light is-block has-text-centered-mobile has-text-weight-normal is-hoverable"
  
  return (
    <footer className="footer has-background-black has-text-grey-light py-6">
      <div className="container">
        <div className="columns is-centered is-mobile is-multiline">
          <div className="column is-narrow has-text-centered-mobile">
            <Link href="/" className={`${linkClasses} navbar-item`}>
              Darkan
            </Link>
          </div>

          <div className="column is-narrow has-text-centered-mobile">
            <a 
              href="https://gitlab.com/darkanrs/client-loader/-/releases"
              target="_blank"
              rel="noopener noreferrer"
              className={`${linkClasses} navbar-item`}
            >
              Download
            </a>
          </div>

          <div className="column is-narrow has-text-centered-mobile">
            <Link 
              href="/highscores/all/1" 
              className={`${linkClasses} navbar-item`}
            >
              Highscores
            </Link>
          </div>

          <div className="column is-narrow has-text-centered-mobile">
            <a 
              href="https://discordapp.com/invite/Z32ggEB"
              target="_blank"
              rel="noopener noreferrer"
              className={`${linkClasses} navbar-item`}
            >
              Discord
            </a>
          </div>

          <div className="column is-narrow has-text-centered-mobile">
            <a 
              href="https://gitlab.com/darkanrs"
              target="_blank"
              rel="noopener noreferrer"
              className={`${linkClasses} navbar-item has-text-warning`}
            >
              Source Code
            </a>
          </div>
        </div>

        <div className="columns is-centered mt-4">
          <div className="column is-narrow has-text-centered">
            <p className="has-text-grey-light mb-2">
              No in-game purchases
            </p>
            <p className="has-text-grey-light">
              Darkan © {new Date().getFullYear()}
            </p>
          </div>
        </div>
      </div>
    </footer>
  )
}