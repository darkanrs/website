import { User, ChevronDown, LogOut, MessageSquare } from 'lucide-react'
import Link from 'next/link'

interface UserMenuProps {
  username: string
  onLogout: () => void
}

export function UserMenu({ username, onLogout }: UserMenuProps) {
  return (
    <div className="navbar-item has-dropdown is-hoverable">
      <a className="navbar-link is-arrowless has-text-grey-lighter">
        <span className="icon mr-2">
          <User size={18} />
        </span>
        <span>{username}</span>
        <span className="icon ml-1">
          <ChevronDown size={16} />
        </span>
      </a>

      <div className="navbar-dropdown is-right has-background-black">
        <Link href="/account" className="navbar-item has-text-grey-lighter">
          <span className="icon mr-2">
            <User size={16} />
          </span>
          <span>Account</span>
        </Link>
        
        <Link href="/friends-chat" className="navbar-item has-text-grey-lighter">
          <span className="icon mr-2">
            <MessageSquare size={16} />
          </span>
          <span>Friend Chat</span>
        </Link>
        
        <hr className="navbar-divider" />
        
        <a 
          onClick={onLogout} 
          className="navbar-item has-text-danger"
        >
          <span className="icon mr-2">
            <LogOut size={16} />
          </span>
          <span>Logout</span>
        </a>
      </div>
    </div>
  )
}