import { useState } from 'react'

interface LoginModalProps {
  onClose: () => void
  onLogin: (username: string, password: string) => Promise<void>
}

export function LoginModal({ onClose, onLogin }: LoginModalProps) {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault()
    try {
      await onLogin(username, password)
      setUsername('')
      setPassword('')
      onClose()
    } catch (error) {
      setError('Login failed. Please check your credentials.')
      console.error(error)
    }
  }

  return (
    <div className="modal is-active">
      <div className="modal-background" onClick={onClose}></div>
      <div className="modal-content is-max-desktop px-4">
        <div className="box has-background-black p-5">
          <div className="is-flex is-justify-content-space-between is-align-items-center mb-4">
            <h2 className="title is-4 has-text-white mb-0">Login to Darkan</h2>
            <button 
              onClick={onClose}
              className="delete is-medium"
              aria-label="close"
            ></button>
          </div>
          
          <form onSubmit={handleSubmit}>
            {error && (
              <div className="notification is-danger is-light py-3 mb-4">
                <p>{error}</p>
              </div>
            )}
            
            <div className="field">
              <div className="control">
                <input
                  type="text"
                  placeholder="Username"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                  className="input is-medium has-background-black-ter has-text-grey-lighter"
                />
              </div>
            </div>
            
            <div className="field">
              <div className="control">
                <input
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  className="input is-medium has-background-black-ter has-text-grey-lighter"
                />
              </div>
            </div>
            
            <div className="field">
              <div className="control">
                <button
                  type="submit"
                  className="button is-warning is-fullwidth is-medium"
                >
                  Login
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}