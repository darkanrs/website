import { UserData } from '@/types'
import { useState, useEffect, useCallback } from 'react'

export function useAuth() {
  const [isLoggedIn, setIsLoggedIn] = useState(false)
  const [user, setUser] = useState<string | null>(null)
  const [isLoading, setIsLoading] = useState(true) 
  
  const handleLogout = useCallback(() => {
    localStorage.removeItem('darkan_token')
    setIsLoggedIn(false)
    setUser(null)
  }, [])

  const validateToken = useCallback(async (token: string): Promise<boolean> => {
    try {
      const response = await fetch('https://api.darkan.org/v1/accounts', {
        headers: { 'Authorization': `Bearer ${token}` }
      })

      if (response.ok) {
        const userData = await response.json() as UserData
        setIsLoggedIn(true)
        setUser(userData.displayName)
        return true
      }
      handleLogout()
      return false
    } catch {
      handleLogout()
      return false
    }
  }, [handleLogout])

  const handleLogin = useCallback(async (username: string, password: string) => {
    try {
      const response = await fetch('https://api.darkan.org/v1/accounts/login', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ username, password }),
      })

      if (!response.ok) throw new Error()

      const { access_token } = await response.json()
      if (!access_token) throw new Error()

      localStorage.setItem('darkan_token', access_token)
      return await validateToken(access_token)
    } catch {
      handleLogout()
      throw new Error('Login failed')
    }
  }, [validateToken, handleLogout])

  useEffect(() => {
    const token = localStorage.getItem('darkan_token')
    if (token)
      validateToken(token).finally(() => setIsLoading(false))
    else
      setIsLoading(false)
  }, [validateToken])

  return { isLoggedIn, user, handleLogin, handleLogout, validateToken, isLoading }
}