import { MessageData } from '@/types'
import { useState, useEffect, useRef, useCallback } from 'react'

export function useWebSocket(url: string) {
  const [messages, setMessages] = useState<MessageData[]>([])
  const [error, setError] = useState('')
  const [fcOwner, setFcOwner] = useState('')
  const socketRef = useRef<WebSocket | null>(null)
  const keepAliveRef = useRef<NodeJS.Timeout | null>(null)
  const connectAttemptRef = useRef(false)

  const handleNewMessage = useCallback((messageData: MessageData) => {
    setFcOwner(messageData.fcOwner)
    setMessages(prev => [...prev, messageData])
  }, [])

  const connect = useCallback(() => {
    if (connectAttemptRef.current || !url) return
    connectAttemptRef.current = true

    if (socketRef.current) {
      socketRef.current.close()
      socketRef.current = null
    }

    if (keepAliveRef.current) {
      clearInterval(keepAliveRef.current)
      keepAliveRef.current = null
    }

    const ws = new WebSocket(url)
    socketRef.current = ws

    ws.onopen = () => {
      setError('')
      keepAliveRef.current = setInterval(() => {
        if (ws.readyState === WebSocket.OPEN)
          ws.send('0')
      }, 5000)
    }

    ws.onmessage = (event) => {
      try {
        const data = JSON.parse(event.data)
        if (data.type === 'FCMessageEvent') handleNewMessage(data.data)
      } catch (error) {
        console.error('Error parsing message:', error)
      }
    }

    ws.onclose = () => {
      connectAttemptRef.current = false
      if (socketRef.current === ws) {
        setTimeout(() => connect(), 1000)
      }
    }

    ws.onerror = () => {
      if (socketRef.current === ws)
        setError('Connection error. Please refresh the page.')
    }
  }, [url, handleNewMessage])

  useEffect(() => {
    connectAttemptRef.current = false
    connect()

    return () => {
      connectAttemptRef.current = false
      if (keepAliveRef.current) {
        clearInterval(keepAliveRef.current)
        keepAliveRef.current = null
      }
      if (socketRef.current) {
        socketRef.current.close()
        socketRef.current = null
      }
    }
  }, [connect])

  const sendMessage = useCallback((message: string) => {
    if (message.trim() && socketRef.current?.readyState === WebSocket.OPEN) {
      socketRef.current.send(JSON.stringify({
        event: 'fc-message',
        data: { message }
      }))
      return true
    }
    return false
  }, [])

  return { messages, error, fcOwner, sendMessage }
}