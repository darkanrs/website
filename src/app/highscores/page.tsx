'use client'

import { useState, useEffect, useCallback } from 'react'
import Image from 'next/image'
import { ChevronLeft, ChevronRight, ChevronDown } from 'lucide-react'

interface Player {
  displayName: string
  ironman: boolean
  totalLevel: number
  totalXp: number
  xp: number[]
}

const SKILL_NAME = ["Attack", "Defence", "Strength", "Constitution", "Ranged", "Prayer", "Magic", "Cooking", "Woodcutting", "Fletching", "Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblore", "Agility", "Thieving", "Slayer", "Farming", "Runecrafting", "Hunter", "Construction", "Summoning", "Dungeoneering"]
const SKILL_XP = [-1, 0, 83, 174, 276, 388, 512, 650, 801, 969, 1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470, 5018, 5624, 6291, 7028, 7842, 8740, 9730, 10_824, 12031, 13363, 14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648, 37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014, 91721, 101333, 111945, 123660, 136594, 150872, 166636, 184_040, 203254, 224466, 247886, 273742, 302288, 333804, 368_599, 407015, 449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257, 992895, 1096278, 1210421, 1336443, 1475581, 1_629_200, 1798808, 1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792, 3972294, 4385776, 4842295, 5_346_332, 5902831, 6517253, 7195629, 7944614, 8771558, 9684577, 10692629, 11805606, 13_034_431, 14391160, 15889109, 17542976, 19368992, 21385073, 23611006, 26068632, 28782069, 31777943, 35085654, 38737661, 42769801, 47221641, 52136869, 57563718, 63555443, 70_170840, 77_474828, 85_539082, 94_442_737, 104_273_167]

const SKILL_ICONS = [
  { name: 'Overall', icon: '/skill-icons/Overall.png' },
  ...SKILL_NAME.map(skill => ({
    name: skill,
    icon: `/skill-icons/${skill}.png`
  }))
]

function getSkillLevelByXP(targetXp: number, skillID: number): number {
  const top = skillID === 24 ? 120 : 99
  for (let i = top; i > 0; i--) {
    if (targetXp > SKILL_XP[i])
      return i
  }
  return 1
}

function formatNumber(num: number): string {
  return num.toLocaleString()
}

export default function HighscoresPage() {
  const [userData, setUserData] = useState<Player[]>([])
  const [isIronHS, setIsIronHS] = useState(false)
  const [selectedSkill, setSelectedSkill] = useState<number>(-1) // -1 for overall
  const [page, setPage] = useState(1)
  const [isLoading, setIsLoading] = useState(true)
  const limit = 10

  const fetchHighscores = useCallback(async () => {
    setIsLoading(true)
    try {
      const gamemode = isIronHS ? "ironman" : "all"
      const skillParam = selectedSkill >= 0 ? `&skill=${selectedSkill}` : ''
      const response = await fetch(`https://api.darkan.org/v1/highscores/remake?page=${page}&limit=${limit}&gamemode=${gamemode}${skillParam}`)
      const data = await response.json()
      setUserData(data)
    } catch (error) {
      console.error('Failed to fetch highscores:', error)
    }
    setIsLoading(false)
  }, [page, isIronHS, selectedSkill])

  useEffect(() => {
    fetchHighscores()
  }, [fetchHighscores])

  return (
    <div className="section">
      <div className="container is-max-widescreen">
        <div className="is-flex is-justify-content-space-between is-align-items-center mb-6">
          <h1 className="title is-2 has-text-white mb-0">
            {isIronHS ? 'Ironman' : 'Global'} Highscores
          </h1>

          <div className="buttons">
            <div className="dropdown is-right mr-2 is-hoverable">
              <div className="dropdown-trigger">
                <button className="button" aria-haspopup="true" aria-controls="skills-menu">
                  <span className="is-flex is-align-items-center">
                    <Image
                      src={SKILL_ICONS[selectedSkill + 1].icon}
                      alt={SKILL_ICONS[selectedSkill + 1].name}
                      width={16}
                      height={16}
                      className="mr-2"
                      priority
                    />
                    {SKILL_ICONS[selectedSkill + 1].name}
                  </span>
                  <span className="icon is-small">
                    <ChevronDown size={16} />
                  </span>
                </button>
              </div>
              <div className="dropdown-menu" id="skills-menu" role="menu">
                <div className="dropdown-content" style={{ maxHeight: '400px', overflowY: 'auto' }}>
                  {SKILL_ICONS.map((skill, index) => (
                    <a
                      key={skill.name}
                      className={`dropdown-item is-flex is-align-items-center ${index - 1 === selectedSkill ? 'is-active' : ''}`}
                      onClick={() => setSelectedSkill(index - 1)}
                    >
                      <Image
                        src={skill.icon}
                        alt={skill.name}
                        width={16}
                        height={16}
                        className="mr-2"
                      />
                      {skill.name}
                    </a>
                  ))}
                </div>
              </div>
            </div>
            <button
              className={`button ${!isIronHS ? 'is-warning' : ''}`}
              onClick={() => setIsIronHS(false)}
            >
              Global
            </button>
            <button
              className={`button ${isIronHS ? 'is-warning' : ''}`}
              onClick={() => setIsIronHS(true)}
            >
              Ironman
            </button>
          </div>
        </div>

        <div className="box" style={{
          background: 'rgba(255, 255, 255, 0.03)',
          borderColor: 'rgba(255, 255, 255, 0.1)'
        }}>
          {isLoading ? (
            <div className="has-text-centered py-6">
              <span className="has-text-grey-lighter">Loading...</span>
            </div>
          ) : (
            <>
              <div className="table-container" style={{ maxHeight: 'calc(100vh - 24rem)', overflowY: 'auto' }}>
                <table className="table is-fullwidth">
                  <thead style={{ position: 'sticky', top: 0, background: 'rgb(5,5,5)', zIndex: 1 }}>
                    <tr>
                      <th className="has-text-grey-lighter" style={{ minWidth: '60px' }}>Rank</th>
                      <th className="has-text-grey-lighter" style={{ minWidth: '150px' }}>Player</th>
                      {selectedSkill === -1 ? (
                        <>
                          <th className="has-text-grey-lighter" style={{ minWidth: '100px' }}>Total Level</th>
                          <th className="has-text-grey-lighter" style={{ minWidth: '120px' }}>Total XP</th>
                        </>
                      ) : (
                        <>
                          <th className="has-text-grey-lighter" style={{ minWidth: '100px' }}>Level</th>
                          <th className="has-text-grey-lighter" style={{ minWidth: '120px' }}>XP</th>
                        </>
                      )}
                    </tr>
                  </thead>
                  <tbody>
                    {userData.map((player, index) => (
                      <tr
                        key={player.displayName}
                        style={{
                          backgroundColor: index % 2 === 0 ? 'rgba(0, 0, 0, 0.2)' : 'transparent'
                        }}
                      >
                        <td className="has-text-grey-lighter">
                          {(page - 1) * limit + index + 1}
                        </td>
                        <td>
                          <span className={player.ironman ? 'has-text-info' : 'has-text-warning'}>
                            {player.displayName}
                          </span>
                        </td>
                        {selectedSkill === -1 ? (
                          <>
                            <td className="has-text-grey-lighter">
                              {formatNumber(player.totalLevel)}
                            </td>
                            <td className="has-text-grey-lighter">
                              {formatNumber(player.totalXp)}
                            </td>
                          </>
                        ) : (
                          <>
                            <td className="has-text-grey-lighter">
                              {getSkillLevelByXP(player.xp[selectedSkill], selectedSkill)}
                            </td>
                            <td className="has-text-grey-lighter">
                              {formatNumber(player.xp[selectedSkill])}
                            </td>
                          </>
                        )}
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>

              <div className="is-flex is-justify-content-center mt-4">
                <nav className="pagination">
                  <button
                    className="pagination-previous button is-small"
                    onClick={() => setPage(p => Math.max(1, p - 1))}
                    disabled={page === 1}
                  >
                    <ChevronLeft size={16} />
                  </button>
                  <button
                    className="pagination-next button is-small"
                    onClick={() => setPage(p => p + 1)}
                    disabled={userData.length < limit}
                  >
                    <ChevronRight size={16} />
                  </button>
                  <ul className="pagination-list">
                    <li>
                      <span className="has-text-grey-lighter">
                        Page {page}
                      </span>
                    </li>
                  </ul>
                </nav>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  )
}