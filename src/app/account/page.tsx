'use client'

import { useState, useEffect } from 'react'
import { useRouter } from 'next/navigation'
import { User, Key, AlertCircle } from 'lucide-react'
import { useAuth } from '@/hooks/useAuth'

interface UserData {
  username: string
  email: string
  rights: string
  displayName: string
}

export default function AccountPage() {
  const router = useRouter()
  const { isLoggedIn, validateToken, isLoading } = useAuth()
  const [, setUserData] = useState<UserData | null>(null)
  const [loading, setLoading] = useState(true)
  const [error, setError] = useState('')
  const [success, setSuccess] = useState('')
  
  const [displayName, setDisplayName] = useState('')
  const [currentPassword, setCurrentPassword] = useState('')
  const [newPassword, setNewPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')

  useEffect(() => {
    const fetchUserData = async () => {
      if (!isLoading && !isLoggedIn) {
        router.push('/')
        return
      }

      if (isLoading) return

      const token = localStorage.getItem('darkan_token')
      if (!token) {
        router.push('/')
        return
      }

      try {
        const response = await fetch('https://api.darkan.org/v1/accounts', {
          headers: { 'Authorization': `Bearer ${token}` }
        })

        if (!response.ok) throw new Error('Failed to fetch user data')

        const data = await response.json() as UserData
        setUserData(data)
        setDisplayName(data.displayName || '')
      } catch (error: unknown) {
        const errorMessage = error instanceof Error ? error.message : 'An error occurred'
        console.error('Error fetching user data:', errorMessage)
        setError(errorMessage)
        router.push('/')
      } finally {
        setLoading(false)
      }
    }

    fetchUserData()
  }, [router, isLoggedIn, isLoading])

  const handleUpdateDisplayName = async (e: React.FormEvent) => {
    e.preventDefault()
    setError('')
    setSuccess('')

    const token = localStorage.getItem('darkan_token')
    if (!token || !await validateToken(token)) return

    try {
      const response = await fetch('https://api.darkan.org/v1/accounts/display-name', {
        method: 'PUT',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ displayName }),
      })

      if (!response.ok) throw new Error('Failed to update display name')

      setSuccess('Display name updated successfully')
    } catch (error: unknown) {
      const errorMessage = error instanceof Error ? error.message : 'Failed to update display name'
      setError(errorMessage)
    }
  }

  const handleUpdatePassword = async (e: React.FormEvent) => {
    e.preventDefault()
    setError('')
    setSuccess('')

    if (newPassword !== confirmPassword) {
      setError('New passwords do not match')
      return
    }

    const token = localStorage.getItem('darkan_token')
    if (!token || !await validateToken(token)) return

    try {
      const response = await fetch('https://api.darkan.org/v1/accounts/password', {
        method: 'PUT',
        headers: {
          'Authorization': `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          currentPassword,
          newPassword,
        }),
      })

      if (!response.ok) throw new Error('Failed to update password')

      setSuccess('Password updated successfully')
      setCurrentPassword('')
      setNewPassword('')
      setConfirmPassword('')
    } catch (error: unknown) {
      const errorMessage = error instanceof Error ? error.message : 'Failed to update password'
      setError(errorMessage)
    }
  }

  if (loading) {
    return (
      <div className="section is-flex is-justify-content-center is-align-items-center" style={{ minHeight: '50vh' }}>
        <div className="loader" />
      </div>
    )
  }

  return (
    <div className="section">
      <div className="container is-max-desktop">
        <h1 className="title is-2 has-text-white mb-6">Account Settings</h1>

        {error && (
          <div className="notification is-danger is-light is-flex is-align-items-center">
            <AlertCircle className="mr-2" style={{ width: '1.25rem', height: '1.25rem' }} />
            <span>{error}</span>
          </div>
        )}

        {success && (
          <div className="notification is-success is-light is-flex is-align-items-center">
            <AlertCircle className="mr-2" style={{ width: '1.25rem', height: '1.25rem' }} />
            <span>{success}</span>
          </div>
        )}

        <div className="content">
          <section className="box" style={{ background: 'rgba(255, 255, 255, 0.03)', borderColor: 'rgba(255, 255, 255, 0.1)' }}>
            <h2 className="title is-4 has-text-white is-flex is-align-items-center">
              <User className="mr-2" style={{ width: '1.25rem', height: '1.25rem' }} />
              Display Name
            </h2>
            <form onSubmit={handleUpdateDisplayName}>
              <div className="field">
                <label className="label has-text-white">New Display Name</label>
                <div className="control">
                  <input
                    type="text"
                    value={displayName}
                    onChange={(e) => setDisplayName(e.target.value)}
                    className="input is-dark"
                    style={{ background: 'rgba(0, 0, 0, 0.2)', borderColor: 'rgba(255, 255, 255, 0.1)' }}
                    minLength={3}
                    maxLength={12}
                    required
                  />
                </div>
                <p className="help">3-12 characters long</p>
              </div>
              <button
                type="submit"
                className="button"
                style={{ 
                  backgroundColor: 'rgb(240,202,138)', 
                  color: 'black',
                  transition: 'background-color 0.3s'
                }}
              >
                Update Display Name
              </button>
            </form>
          </section>

          <section className="box mt-6" style={{ background: 'rgba(255, 255, 255, 0.03)', borderColor: 'rgba(255, 255, 255, 0.1)' }}>
            <h2 className="title is-4 has-text-white is-flex is-align-items-center">
              <Key className="mr-2" style={{ width: '1.25rem', height: '1.25rem' }} />
              Change Password
            </h2>
            <form onSubmit={handleUpdatePassword}>
              <div className="field">
                <label className="label has-text-white">Current Password</label>
                <div className="control">
                  <input
                    type="password"
                    value={currentPassword}
                    onChange={(e) => setCurrentPassword(e.target.value)}
                    className="input is-dark"
                    style={{ background: 'rgba(0, 0, 0, 0.2)', borderColor: 'rgba(255, 255, 255, 0.1)' }}
                    required
                  />
                </div>
              </div>
              <div className="field">
                <label className="label has-text-white">New Password</label>
                <div className="control">
                  <input
                    type="password"
                    value={newPassword}
                    onChange={(e) => setNewPassword(e.target.value)}
                    className="input is-dark"
                    style={{ background: 'rgba(0, 0, 0, 0.2)', borderColor: 'rgba(255, 255, 255, 0.1)' }}
                    minLength={8}
                    required
                  />
                </div>
                <p className="help">Minimum 8 characters</p>
              </div>
              <div className="field">
                <label className="label has-text-white">Confirm New Password</label>
                <div className="control">
                  <input
                    type="password"
                    value={confirmPassword}
                    onChange={(e) => setConfirmPassword(e.target.value)}
                    className="input is-dark"
                    style={{ background: 'rgba(0, 0, 0, 0.2)', borderColor: 'rgba(255, 255, 255, 0.1)' }}
                    minLength={8}
                    required
                  />
                </div>
              </div>
              <button
                type="submit"
                className="button"
                style={{ 
                  backgroundColor: 'rgb(240,202,138)', 
                  color: 'black',
                  transition: 'background-color 0.3s'
                }}
              >
                Update Password
              </button>
            </form>
          </section>
        </div>
      </div>
    </div>
  )
}