import type { Metadata } from 'next';
import { Geist, Geist_Mono } from 'next/font/google';
import './globals.css';
import Header from '@/components/Header';
import Footer from '@/components/Footer';

const geistSans = Geist({
  variable: '--font-geist-sans',
  subsets: ['latin'],
});

const geistMono = Geist_Mono({
  variable: '--font-geist-mono',
  subsets: ['latin'],
});

export const metadata: Metadata = {
  title: 'Darkan',
  description: 'The best open-source game-server project available.',
  icons: {
    icon: '/favicon.ico',
  },
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang='en' className={`${geistSans.variable} ${geistMono.variable}`}>
      <body style={{
        minHeight: '100vh',
        background: 'linear-gradient(to bottom, rgb(10,10,10), rgb(20,20,20))',
        color: '#b5b5b5'
      }}>
        <Header />
        <div className='container' style={{
          minHeight: '80vh',
          paddingTop: '4rem'
        }}>
          {children}
        </div>
        <Footer />
      </body>
    </html>
  );
}