'use client'

import { useState, useRef, useEffect } from 'react'
import { useRouter } from 'next/navigation'
import { Send, AlertCircle } from 'lucide-react'
import Image from 'next/image'
import { useWebSocket } from '@/hooks/useWebsocket'

const MAX_MESSAGE_LENGTH = 210
const WEBSOCKET_URL = 'wss://api.darkan.org/v1/social/ws'

const rankImages = {
  OWNER: '/mod_gold.gif',
  DEVELOPER: '/mod_gold.gif',
  ADMIN: '/mod_gold.gif',
  MOD: '/mod_silver.gif'
} as const

export default function ChatPage() {
  const router = useRouter()
  const [messageInput, setMessageInput] = useState('')
  const messageContainerRef = useRef<HTMLDivElement>(null)
  
  const token = typeof window !== 'undefined' ? localStorage.getItem('darkan_token') : null
  const { messages, error, fcOwner, sendMessage } = useWebSocket(
    token ? `${WEBSOCKET_URL}?token=${token}` : ''
  )

  useEffect(() => {
    if (!token) {
      router.push('/')
      return
    }
  }, [token, router])

  useEffect(() => {
    const container = messageContainerRef.current
    if (container) {
      container.scrollTop = container.scrollHeight - container.clientHeight
    }
  }, [messages])

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    if (sendMessage(messageInput))
      setMessageInput('')
  }

  const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value
    if (value.length <= MAX_MESSAGE_LENGTH)
      setMessageInput(value)
  }

  return (
    <div className="section">
      <div className="container is-max-desktop">

        {error && (
          <div className="notification is-danger is-light is-flex is-align-items-center">
            <AlertCircle className="mr-2" style={{ width: '1.25rem', height: '1.25rem' }} />
            <span>{error}</span>
          </div>
        )}

        <div className="content">
          <section 
            className="box" 
            style={{ 
              background: 'rgba(255, 255, 255, 0.03)', 
              borderColor: 'rgba(255, 255, 255, 0.1)',
              height: 'calc(100vh - 16rem)',
              display: 'flex',
              flexDirection: 'column',
              padding: 0
            }}
          >
            <header className="p-4" style={{ borderBottom: '1px solid rgba(255, 255, 255, 0.1)' }}>
              <h2 className="title is-4 has-text-white mb-0">
                {fcOwner ? `${fcOwner}'s Chat` : 'Friends Chat'}
              </h2>
            </header>

            <div 
              ref={messageContainerRef}
              className="p-4"
              style={{
                flex: 1,
                overflowY: 'auto',
                display: 'flex',
                flexDirection: 'column',
                gap: '0.5rem'
              }}
            >
              {messages.map((msg, index) => (
                <div 
                  key={index}
                  className="p-2"
                  style={{
                    background: 'rgba(0, 0, 0, 0.2)',
                    borderRadius: '4px'
                  }}
                >
                  <div className="is-flex" style={{ gap: '0.5rem' }}>
                    <div className="is-flex is-align-items-center is-flex-shrink-0" style={{ gap: '0.5rem' }}>
                      {msg.srcRights in rankImages && (
                        <Image
                          src={rankImages[msg.srcRights as keyof typeof rankImages]}
                          alt="rank"
                          width={16}
                          height={16}
                        />
                      )}
                      <span className="has-text-weight-medium has-text-white">
                        {msg.srcName}:
                      </span>
                    </div>
                    <span className="has-text-grey-lighter" style={{ wordBreak: 'break-word' }}>{msg.message}</span>
                  </div>
                </div>
              ))}
            </div>

            <form 
              onSubmit={handleSubmit}
              className="p-4"
              style={{ borderTop: '1px solid rgba(255, 255, 255, 0.1)' }}
            >
              <div className="field has-addons">
                <div className="control is-expanded">
                  <div style={{ position: 'relative' }}>
                    <input
                      type="text"
                      value={messageInput}
                      onChange={handleInputChange}
                      placeholder="Type a message..."
                      className="input"
                      style={{
                        background: 'rgba(0, 0, 0, 0.2)',
                        borderColor: 'rgba(255, 255, 255, 0.1)',
                        color: 'white'
                      }}
                    />
                    <span 
                      className="has-text-grey-lighter"
                      style={{
                        position: 'absolute',
                        right: '0.75rem',
                        top: '50%',
                        transform: 'translateY(-50%)',
                        fontSize: '0.875rem'
                      }}
                    >
                      {MAX_MESSAGE_LENGTH - messageInput.length}
                    </span>
                  </div>
                </div>
                <div className="control">
                  <button
                    type="submit"
                    className="button"
                    style={{
                      backgroundColor: 'rgb(240,202,138)',
                      color: 'black'
                    }}
                  >
                    <Send size={16} />
                  </button>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  )
}