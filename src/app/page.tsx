import { Download } from 'lucide-react'
import Image from 'next/image'
import Link from 'next/link'

export default function LandingPage() {
  return (
    <>
      <div style={{ height: '85vh' }}>
        <div className="has-text-centered" style={{ paddingTop: '20vh', position: 'relative' }}>
          <Image
            src="/dev-bg.jpg"
            alt="development"
            width={800}
            height={450}
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              zIndex: -1,
              opacity: 0.1,
              objectFit: 'cover',
              width: '100vw'
            }}
            priority
          />
          <div>
            <h1 className="title is-1 has-text-white mb-2">Darkan</h1>
            <p className="subtitle is-4 has-text-weight-semibold mb-6"
              style={{
                color: 'rgb(222,222,222)',
                paddingLeft: '15vw',
                paddingRight: '15vw'
              }}>
              The best open-source game-server project available.
            </p>
            <div>

              <a href="https://gitlab.com/darkanrs/client-loader/-/releases"
                target="_blank"
                rel="noopener noreferrer"
                className="button is-large my-6"
                style={{
                  color: 'rgb(240,202,138)',
                  background: 'rgba(255, 255, 255, 0.06)',
                  border: '1px solid rgba(255, 255, 255, 0.1)',
                  backdropFilter: 'blur(16px)',
                  padding: '1.5rem 2.5rem',
                  transition: 'background-color 0.3s'
                }}
              >
                Play Now
              </a>
            </div>
            <p className="is-size-7 mt-6" style={{ color: 'rgb(222,222,222)' }}>
              Non-profit, no pay to win
            </p>
          </div>
        </div>
      </div>

      <section>
        <div className="is-flex is-justify-content-center">
          <div style={{ width: '100vw', color: 'rgb(222,222,222)' }}>
            <div className="has-text-centered py-6" style={{ padding: '0 5vw' }}>
              <h2 className="title is-3 mx-auto mb-6"
                style={{ maxWidth: '60rem' }}>
                The only codebase supporting multi-world connections through a proper lobby server.
              </h2>
              <div className="is-relative mx-auto" style={{ maxWidth: '64rem' }}>
                <Image
                  src="/lobby.webp"
                  alt="client"
                  width={800}
                  height={450}
                  className="image"
                  style={{
                    width: '100%',
                    boxShadow: '0 -25px 75px rgb(0,0,0)'
                  }}
                />
                <div style={{
                  position: 'absolute',
                  inset: 0,
                  background: 'linear-gradient(to bottom, transparent, rgba(0,0,0,0.8), black)'
                }}></div>
              </div>
              <div className="mt-6">
                <p className="is-size-4">Stay connected with our community!</p>
              </div>
              <div className="mx-auto mt-4" style={{ maxWidth: '25rem' }}>

                <a href="https://discordapp.com/invite/Z32ggEB"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="is-inline-block"
                >
                  <Image
                    src="/social/discord-brands.svg"
                    alt="discord"
                    width={50}
                    height={50}
                    style={{
                      width: '37.5px',
                      height: '37.5px',
                      filter: 'invert(1)',
                      transition: 'transform 0.25s'
                    }}
                    className="hover-lift"
                  />
                </a>
              </div>
            </div>

            {/* World 1 */}
            <div style={{ backgroundColor: 'rgb(5,5,5)', paddingBottom: '5.75rem' }}>
              <div className="py-6 px-4">
                <h3 className="title is-3 has-text-centered mb-0"
                  style={{ color: 'rgb(240,202,138)' }}>
                  World 1
                </h3>
                <h4 className="subtitle is-4 has-text-centered has-text-white mb-4">
                  2012 Remake
                </h4>
                <p className="has-text-centered is-size-5 mx-auto"
                  style={{ maxWidth: '70rem' }}>
                  As a complete remake, players are able to{' '}

                  <a href="https://gitlab.com/darkanrs/client-loader/-/releases"
                    style={{
                      color: 'rgb(240,202,138)',
                      transition: 'color 0.3s'
                    }}
                    className="hover-white"
                  >
                    play through
                  </a>{' '}
                  a variety of beginner to expert level skills, tasks, quests, and more exactly as it was in 2012.
                </p>
              </div>
              <div className="is-relative mx-auto" style={{ maxWidth: '64rem' }}>
                <Image
                  src="/w1_banner.webp"
                  alt="client"
                  width={800}
                  height={450}
                  className="image"
                  style={{
                    width: '100%',
                    boxShadow: '0 -25px 75px rgb(0,0,0)'
                  }}
                />
                <div style={{
                  position: 'absolute',
                  inset: 0,
                  background: 'linear-gradient(to bottom, transparent, rgba(0,0,0,0.8), black)'
                }}></div>
              </div>
            </div>

            <div style={{ backgroundColor: 'rgb(5,5,5)' }} className="px-6 pb-6">
              <ul className="is-size-5 has-text-centered p-0 mt-0">
                <div className="columns is-centered is-mobile is-multiline">
                  <li className="column is-narrow mb-2">Full-game development</li>
                  <li className="column is-narrow mb-2">QOL additions</li>
                  <li className="column is-narrow mb-2">Constant updates</li>
                  <li className="column is-narrow">Loyalty system</li>
                </div>
              </ul>
            </div>

            <div style={{ backgroundColor: 'rgb(20,20,20)' }} className="has-text-centered py-6 px-4">
              <h2 className="title is-3 has-text-white mt-0">Rank from noob to #1</h2>
              <p className="is-size-5 mx-auto" style={{ maxWidth: '50rem' }}>
                If you&apos;re all about EXP, then spectate the{' '}
                <Link
                  href="/highscores"
                  style={{
                    color: 'rgb(240,202,138)',
                    transition: 'color 0.3s'
                  }}
                  className="hover-white"
                >
                  highscores
                </Link>{' '}
                to see who&apos;s not EXP wasting.
              </p>
            </div>

            {/* World 2 */}
            <div style={{ backgroundColor: 'rgb(5,5,5)', paddingBottom: '5.75rem' }}>
              <div className="py-6 px-4">
                <h3 className="title is-3 has-text-centered mb-0"
                  style={{ color: 'rgb(240,202,138)' }}>
                  World 2
                </h3>
                <h4 className="subtitle is-4 has-text-centered has-text-white mb-4">
                  Darkan Accelerated
                </h4>
                <p className="has-text-centered is-size-5 mx-auto"
                  style={{ maxWidth: '70rem' }}>
                  What happens when you add 25x EXP rates and custom content to World 1? You get World 2.
                </p>
              </div>
              <div className="is-relative mx-auto" style={{ maxWidth: '64rem' }}>
                <Image
                  src="/w2_banner.webp"
                  alt="client"
                  width={800}
                  height={450}
                  className="image"
                  style={{
                    width: '100%',
                    boxShadow: '0 -25px 75px rgb(0,0,0)'
                  }}
                />
                <div style={{
                  position: 'absolute',
                  inset: 0,
                  background: 'linear-gradient(to bottom, transparent, rgba(0,0,0,0.8), black)'
                }}></div>
              </div>
            </div>

            <div style={{ backgroundColor: 'rgb(5,5,5)' }} className="px-6 pb-6">
              <ul className="is-size-5 has-text-centered p-0 mt-0">
                <div className="columns is-centered is-mobile is-multiline">
                  <li className="column is-narrow mb-2">25x EXP</li>
                  <li className="column is-narrow mb-2">Home</li>
                  <li className="column is-narrow mb-2">Supply Shop</li>
                  <li className="column is-narrow mb-2">Slayer Portals</li>
                  <li className="column is-narrow">And more!</li>
                </div>
              </ul>
            </div>

            <div className="py-6">
              <h2 className="title is-3 has-text-centered has-text-white mb-0">
                A community project
              </h2>
              <p className="has-text-centered is-size-5 mx-auto px-4 mb-0"
                style={{ maxWidth: '50rem' }}>
                Darkan is an{' '}

                <a href="https://gitlab.com/DarkanRS"
                  target="_blank"
                  rel="noopener noreferrer"
                  style={{
                    color: 'rgb(240,202,138)',
                    transition: 'color 0.3s'
                  }}
                  className="hover-white"
                >
                  open source project
                </a>{' '}
                with a community dedicated to helping people learn software engineering and enjoy a game no longer available to the public.
              </p>
            </div>

            <div style={{ backgroundColor: 'rgb(25,25,25)' }} className="has-text-centered">
              <div className="py-6 is-size-4">
                <h2 className="m-4 has-text-white">Play Now!</h2>

                <a href="https://gitlab.com/darkanrs/client-loader/-/releases"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="is-inline-block"
                >
                  <Download
                    style={{
                      width: '75px',
                      height: '75px',
                      color: 'rgb(240,202,138)',
                      transition: 'transform 1s',
                      fontWeight: 200
                    }}
                    className="rotate-on-hover"
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}