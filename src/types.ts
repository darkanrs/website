export interface UserData {
  username: string
  email: string
  rights: string
  displayName: string
}

export interface MessageData {
  fcOwner: string
  srcName: string
  message: string
  srcRights: string
}